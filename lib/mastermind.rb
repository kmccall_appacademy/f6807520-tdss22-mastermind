class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    chars = string.chars.map(&:upcase)
    raise ArgumentError unless chars - PEGS.keys == []
    pegs = chars.map { |char| PEGS[char] }
    self.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }

    self.new(pegs)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other)
    matches = 0
    4.times { |idx| matches += 1 if @pegs[idx] == other.pegs[idx] }
    matches
  end

  def near_matches(other)
    matches = 0
    PEGS.values.each do |color|
      matches += [@pegs.count(color), other.pegs.count(color)].min
    end
    matches - exact_matches(other)
  end

  def ==(other)
    return false unless other.class == Code
    @pegs == other.pegs
  end

  def to_s
    "[#{@pegs.join(", ")}]"
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil)
    @secret_code = secret_code || Code.random
  end

  def get_guess
    print 'Please enter a guess (ex. "GBYR"): '
    @guess = Code.parse(gets.chomp)
  end

  def display_matches
    puts "exact matches: #{@secret_code.exact_matches(@guess)}"
    puts "near matches:  #{@secret_code.near_matches(@guess)}"
  end

  def play
    10.times do |turn|
      get_guess
      break if won?
      display_matches
    end
    conclude

  end

  def won?
    @secret_code == @guess
  end

  def conclude
    if @guess == @secret_code
    puts "Woohoo, you win!"
    else
      puts "YOU LOSE"
    end
    puts "THE SECRET CODE WAS #{secret_code} "
  end
end


if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
end
